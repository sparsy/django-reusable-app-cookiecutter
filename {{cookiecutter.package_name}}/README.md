{{cookiecutter.app_title}}
======================

{{cookiecutter.app_description}}

Quick start
-----------

1. Add "{{cookiecutter.app_name}}" to INSTALLED_APPS:

    INSTALLED_APPS = {
        ...
        '{{cookiecutter.app_name}}'
    }

3. Run `python manage.py migrate` to create {{cookiecutter.app_name}}'s models database tables.

4. Run the development server and access http://127.0.0.1:8000/ to use the app.
