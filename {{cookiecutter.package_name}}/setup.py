# -*- encoding: utf-8 -*-

import os
import sys
from setuptools import setup
from {{cookiecutter.app_name}} import __version__

README = open(os.path.join(os.path.dirname(__file__), 'README.md')).read()

# Allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='{{cookiecutter.package_name}}',
    version=__version__,
    packages=['{{cookiecutter.app_name}}'],
    install_requires=[
        'Django',
    ],
    include_package_data=True,
    license='{{cookiecutter.license}}',
    description='{{cookiecutter.app_description}}',
    long_description=README,
    author='{{cookiecutter.author}}',
    author_email='{{cookiecutter.email}}',
    url='{{cookiecutter.url}}',
    keywords=[
        'django',
    ],
    classifiers=[
        'Framework :: Django',
        'Intended Audience :: Developers',
        {% if cookiecutter.license == "MIT" -%}
        'License :: OSI Approved :: MIT License',
        {% elif cookiecutter.license == "BSD-3" -%}
        'License :: OSI Approved :: BSD License',
        {% elif cookiecutter.license == "WTFPL" -%}
        'License :: Public Domain',
        {% elif cookiecutter.license == "Public Domain" -%}
        'License :: Public Domain',
        {% elif cookiecutter.license == "GNU GPL v3.0" -%}
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        {% elif cookiecutter.license == "Apache Software License 2.0" -%}
        'License :: OSI Approved :: Apache Software License',
        {% elif cookiecutter.license == "No License" -%}
        'License :: Other/Proprietary License',
        {% endif -%}
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 2.7',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content'
    ]
)
