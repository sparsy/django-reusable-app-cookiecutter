# -*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function

from cookiecutter.main import cookiecutter

# POST HOOK VARS INIT
context = {
    "app_title": "{{cookiecutter.app_title}}",
    "package_name": "{{cookiecutter.package_name}}",
    "app_name": "{{cookiecutter.app_name}}",
    "app_description": "{{cookiecutter.app_description}}",

    "__license": "{{cookiecutter.license}}",

    "__year": "{{cookiecutter.year}}",
    "__author": "{{cookiecutter.author}}",

    "__version": "{{cookiecutter.version}}",

    # For the app metadata generation
    "__reusable_app": "yes",
}

# POST HOOK
cookiecutter(
    "http://gitlab.contabo.cloud/cookiecutters/django-app-cookiecutter.git",
    no_input=True,
    extra_context=context
)


# POST HOOK FINAL PRINT
print("Reusable app generation completed!")
print("")
